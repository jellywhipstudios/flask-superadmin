import math
import re

from wtforms import fields, widgets
from flask import request, url_for, redirect, flash, abort, session, Response

from flask_superadmin.babel import gettext
from flask_superadmin.base import BaseView, expose
# from flask_superadmin.form import format_form
from flask_superadmin.form import BaseForm, ChosenSelectWidget, DatePickerWidget, \
    DateTimePickerWidget, FileField

import traceback
import inflection

class AdminModelConverter(object):
    def convert(self, *args, **kwargs):
        field = super(AdminModelConverter, self).convert(*args, **kwargs)
        if field:
            widget = field.kwargs.get('widget', field.field_class.widget)
            # print field, widget
            if isinstance(widget, widgets.Select):
                field.kwargs['widget'] = ChosenSelectWidget(multiple=widget.multiple)
            elif issubclass(field.field_class, fields.DateTimeField):
                field.kwargs['widget'] = DateTimePickerWidget()
            elif issubclass(field.field_class, fields.DateField):
                field.kwargs['widget'] = DatePickerWidget()
            elif issubclass(field.field_class, fields.FileField):
                field.field_class = FileField
        return field


first_cap_re = re.compile('(.)([A-Z][a-z]+)')
def camelcase_to_space(name):
    return first_cap_re.sub(r'\1 \2', name)


def prettify(str):
    return str.replace('_', ' ').title()


class BaseModelAdmin(BaseView):
    """
    BaseModelView provides create/edit/delete functionality for a peewee Model.
    """

    # Number of objects to display per page in the list view
    list_per_page = 20

    # Columns to display in the list index - can be field names or callables.
    # Admin's methods have higher priority than the fields/methods on
    # the model or document.
    __list_display__ = tuple()

    @property
    def list_display(self):
        return self.__list_display__ or getattr(self.model, "__list_display__",
            tuple())

    @list_display.setter
    def set_list_display(self, value):
        self.__list_display__ = value

    # form parameters, lists of fields
    exclude = None
    tabs = None
    only = None
    fields = None
    readonly_fields = []
    fields_order = None  # order of the fields in the form. Fields that aren't mentioned here are appended at the end

    form = None

    can_edit = True
    can_create = True
    can_delete = True
    allow_export = True

    list_template = 'admin/model/list.html'
    edit_template = 'admin/model/edit.html'
    add_template = 'admin/model/add.html'
    delete_template = 'admin/model/delete.html'

    search_fields = None
    actions = None

    field_overrides = {}

    # a dictionary of field_name: overridden_params_dict, e.g.
    # { 'first_name': { 'label': 'First', 'description': 'This is first name' } }
    # parameters that can be overridden: label, description, validators, filters, default
    field_args = None

    @staticmethod
    def model_detect(model):
        return False

    def __init__(self, model=None, name=None, category=None, endpoint=None,
                 url=None):
        if name is None:
            name = '%s' % camelcase_to_space(model.__name__)

        if endpoint is None:
            endpoint = ('%s' % model.__name__).lower()

        super(BaseModelAdmin, self).__init__(name, category, endpoint, url)

        if model:
            self.model = model

    @property
    def can_export(self):
        columns = getattr(self.model, "__export_columns__", None)
        if columns:
            return True

        return self.allow_exports and (self.list_display or
            getattr(self.model, "csv"))

    def get_display_name(self):
        return getattr(self.model, "__pretty_name__", inflection.titleize(
            self.model.__name__))

    def get_display_name_plural(self):
        return getattr(self.model, "__pretty_name_plural__",
            inflection.pluralize(self.get_display_name()))

    def allow_pk(self):
        return not self.model._meta.auto_increment

    def get_column(self, instance, name):
        parts = name.split('.')
        value = instance
        for p in parts:

            # admin's methods have higher priority than the fields/methods on
            # the model or document. If a callable is found on the admin
            # level, it's also passed an instance object
            if hasattr(self, p) and callable(getattr(self, p)):
                value = getattr(self, p)(instance)
            else:
                value = getattr(value, p, None)
                if callable(value):
                    value = value()

            if not value:
                break

        return value

    def get_reference(self, column_value):
        for model, model_view in self.admin._models:
            if type(column_value) == model:
                return '/admin/%s/%s/' % (model_view.endpoint, column_value.pk)

    def get_converter(self):
        raise NotImplemented()

    def get_model_form(self):
        """ Returns the model form, should get overridden in backend-specific
        view.
        """
        raise NotImplemented()

    def get_form(self, include_readonly=False):
        excludes = [
            set(self.exclude or []),
            set(getattr(self.model, "__exclude__", [])),
            set(self.tabs or []),
            set(getattr(self.model, "__tabs__", []))
        ]

        if getattr(self.model, "__fields_order__", None):
            self.fields_order = self.model.__fields_order__

        if not include_readonly:
            excludes.append(self.readonly_fields)

        exclude = list(set().union(*excludes))

        only = list(set(self.only or getattr(self.model, "__only__", []))
            - set(exclude or []))

        model_form = self.get_model_form()
        converter = self.get_converter()
        if isinstance(converter, type):
            converter = converter()
        form = model_form(self.model, base_class=BaseForm, only=only,
                          exclude=exclude, field_args=self.field_args,
                          converter=converter)

        form.readonly_fields = self.readonly_fields
        return form

    def get_add_form(self):
        return self.get_form()

    def get_objects(self, *pks):
        raise NotImplemented()

    def get_object(self, pk):
        raise NotImplemented()

    def get_pk(self, instance):
        return

    def save_model(self, instance, form, adding=False):
        raise NotImplemented()

    def delete_models(self, *pks):
        raise NotImplemented()

    def is_sortable(self, column):
        return False

    def field_name(self, field):
        return inflection.titleize(field)

    def construct_search(self, field_name):
        raise NotImplemented()

    def get_queryset(self):
        raise NotImplemented()

    def get_list(self):
        raise NotImplemented()

    def get_url_name(self, name):
        URLS = {
            'index': '.list',
            'add': '.add',
            'delete': '.delete',
            'edit': '.edit'
        }
        return URLS[name]

    def model_with_filter(self, filter):
        return self.model()

    def dispatch_save_redirect(self, instance):
        if '_edit' in request.form:
            return redirect(
                url_for(self.get_url_name('edit'), pk=self.get_pk(instance),
                    filter=self.filter)
            )
        elif '_add_another' in request.form:
            return redirect(url_for(self.get_url_name('add'),
                filter=self.filter))
        else:
            return redirect(url_for(self.get_url_name('index'),
                filter=self.filter))

    @expose('/add/', methods=('GET', 'POST'))
    def add(self):
        Form = self.get_add_form()
        if request.method == 'POST':
            form = Form()
            if form.validate_on_submit():
                try:
                    instance = self.save_model(self.model(), form, True)
                    flash(gettext('New %(model)s saved successfully',
                          model=self.get_display_name()), 'success')
                    return self.dispatch_save_redirect(instance)
                except Exception, ex:
                    print traceback.format_exc()
                    if self.session:
                        self.session.rollback()
                    flash(gettext('Failed to add model. %(error)s',
                          error=str(ex)), 'error')

        else:
            obj = self.model_with_filter(self.filter)
            form = Form(obj=obj)

        return self.render(self.add_template, model=self.model, form=form,
            filter=self.filter)

    @property
    def page(self):
        return request.args.get('page', 0, type=int)

    def total_pages(self, count):
        return int(math.ceil(float(count) / self.list_per_page))

    @property
    def sort(self):
        sort = request.args.get('sort', None)
        if sort and sort.startswith('-'):
            desc = True
            sort = sort[1:]
        else:
            desc = False
        return sort, desc

    @property
    def search(self):
        return request.args.get('q', None)

    @property
    def filter(self):
        return request.args.get("filter", None)

    def page_url(self, page):
        search_query = self.search
        sort, desc = self.sort
        if sort and desc:
            sort = '-' + sort
        if page == 0:
            page = None
        return url_for(self.get_url_name('index'), page=page, sort=sort, q=search_query, filter=self.filter)

    def sort_url(self, sort, desc=None):
        if sort and desc:
            sort = '-' + sort
        search_query = self.search
        return url_for(self.get_url_name('index'), sort=sort, q=search_query, filter=self.filter)

    def get_tab_names(self):
        return getattr(self.model, "__tabs__", self.tabs) or []

    def get_tabs(self, instance):
        return []

    @expose('/', methods=('GET', 'POST',))
    def list(self):
        """
            List view
        """
        # Grab parameters from URL
        if request.method == 'POST':
            id_list = request.form.getlist('_selected_action')
            if id_list and (request.form.get('action-delete') or \
                request.form.get('action', None) == 'delete'):
                return self.delete(*id_list)

        sort, sort_desc = self.sort
        page = self.page
        search_query = self.search
        count, data = self.get_list(page=page, sort=sort, sort_desc=sort_desc,
                                    search_query=search_query,
                                    filter_=self.filter)

        breadcrumbs = self.breadcrumb(self.get_display_name_plural(),
            clear=not self.filter)

        return self.render(self.list_template, data=data, page=page,
                           total_pages=self.total_pages(count), sort=sort,
                           sort_desc=sort_desc, count=count, modeladmin=self,
                           search_query=search_query, filter=self.filter,
                           breadcrumbs=breadcrumbs)

    @expose("/export/csv", methods=('GET', 'POST'))
    def export(self):
        sort, sort_desc = self.sort
        search_query = self.search
        filter_ = self.filter

        def generate():
            count, data = self.get_list(page=0, sort=sort,
                sort_desc=sort_desc, search_query=search_query,
                filter_=filter_)

            pages = count / self.list_per_page

            def escape(val):
                try:
                    float(val)
                    return unicode(val)
                except ValueError:
                    return u"\"{0}\"".format(val.replace('"', '\\"'))

            for page in xrange(pages+1):
                count, data = self.get_list(page=page, sort=sort,
                    sort_desc=sort_desc, search_query=search_query,
                    filter_=filter_)

                for d in data:
                    columns = []
                    if hasattr(d, "csv") and callable(d.csv):
                        columns = list(d.csv())
                    else:
                        columns = list(self.csv(d))

                    yield u",".join([escape(c) for c in columns]) + u"\n"

        response = Response(generate(), mimetype="text/csv")
        response.headers["Content-Disposition"] = (
            'attachment; filename="{0}.csv"'.format(self.get_display_name()))

        return response

    def csv(self, instance):
        columns = getattr(self.model, "__export_columns__", self.list_display)

        for c in columns:
            yield self.get_column(instance, c)

    def breadcrumb(self, title, clear=False):
        breadcrumbs = session.pop("breadcrumbs", [])

        if clear:
            breadcrumbs = [(title, request.url)]
        else:
            if breadcrumbs:
                for index, (name, url) in enumerate(breadcrumbs):
                    if url == request.url:
                        breadcrumbs = breadcrumbs[:index]
                        break

            breadcrumbs.append((title, request.url))

        session["breadcrumbs"] = breadcrumbs
        return breadcrumbs

    @expose('/<pk>/', methods=('GET', 'POST'))
    def edit(self, pk):
        try:
            instance = self.get_object(pk)
        except self.model.DoesNotExist:
            abort(404)

        Form = self.get_form(include_readonly=request.method == 'GET')

        if request.method == 'POST':
            form = Form(obj=instance)
            if form.validate_on_submit():
                try:
                    self.save_model(instance, form, False)
                    flash('Changes to %s saved successfully' % self.get_display_name(),
                          'success')
                    return self.dispatch_save_redirect(instance)
                except Exception, ex:
                    print traceback.format_exc()
                    flash(gettext('Failed to edit model. %(error)s', error=str(ex)),
                          'error')
        else:
            form = Form(obj=instance)

        b = unicode(instance)
        if len(b) > 30:
            b = b[:30] + u"\u2026"

        breadcrumbs = self.breadcrumb(b)

        return self.render(self.edit_template, model=self.model, form=form,
                           pk=self.get_pk(instance), instance=instance,
                           tabs=self.get_tabs(instance), filter=self.filter,
                           breadcrumbs=breadcrumbs)

    @expose('/<pk>/delete', methods=('GET', 'POST'))
    def delete(self, pk=None, *pks):
        if pk:
            pks += pk,
            # collected = {}
            # if self.delete_collect_objects:
            #     for obj in query:
            #         collected[obj.get_pk()] = self.collect_objects(obj)

        if request.method == 'POST' and 'confirm_delete' in request.form:
            count = len(pks)
            self.delete_models(*pks)

            flash('Successfully deleted %s %ss' % (count, self.get_display_name()),
                  'success')
            return redirect(url_for(self.get_url_name('index'),
                filter=self.filter))
        else:
            instances = self.get_objects(*pks)

        return self.render(self.delete_template,
            instances=instances, filter=self.filter
            # query=query,
            # collected=collected,
        )


class ModelAdmin(object):
    pass
