from sqlalchemy.sql.expression import desc, literal
from sqlalchemy import schema, inspect
from wtforms.ext.sqlalchemy.orm import model_form

from flask_superadmin.model.base import BaseModelAdmin

from .orm import AdminModelConverter

import inflection
from flask import url_for
import json
from functools import wraps
from wtforms.fields.core import UnboundField
from wtforms.validators import Required, Optional
from wtforms import Form


class ModelAdmin(BaseModelAdmin):
    hide_backrefs = False

    def __init__(self, model, session=None,
                 *args, **kwargs):
        super(ModelAdmin, self).__init__(model, *args, **kwargs)
        if session:
            self.session = session
        self._primary_key = self.pk_key

    @staticmethod
    def model_detect(model):
        return isinstance(getattr(model, 'metadata', None), schema.MetaData)

    def _get_model_iterator(self, model=None):
        """
            Return property iterator for the model
        """
        if model is None:
            model = self.model

        return model._sa_class_manager.mapper.iterate_properties

    @property
    def pk_key(self):
        for p in self._get_model_iterator():
            if hasattr(p, 'columns'):
                for c in p.columns:
                    if c.primary_key:
                        return p.key

    def allow_pk(self):
        return False

    def get_model_form(self):
        @wraps(model_form)
        def validated_form(model, *args, **kwargs):
            form = model_form.__call__(model, *args, **kwargs)
            form.order_fields(self.fields_order)

            for attr in dir(form):
                form_field = getattr(form, attr)
                if isinstance(form_field, UnboundField):
                    model_attr = getattr(self.model, attr, None)
                    if model_attr:
                        required = getattr(model_attr, "required", None)
                        validators = getattr(model_attr, "validators", None)
                        if required or validators:
                            v = []
                            for validator in form_field.kwargs["validators"]:
                                if not isinstance(validator, Optional):
                                    v.append(validator)

                            if required:
                                v.append(Required())
                            elif validators:
                                v = validators

                            form_field.kwargs["validators"] = v

            return form

        return validated_form

    def get_converter(self):
        return AdminModelConverter(self)

    @property
    def query(self):
        return self.get_queryset()  # TODO remove eventually (kept for backwards compatibility)

    def get_queryset(self):
        return self.session.query(self.model)

    def get_objects(self, *pks):
        id = self.get_pk(self.model)
        return self.get_queryset().filter(id.in_(pks))

    def get_object(self, pk):
        return self.get_queryset().get(pk)

    def get_pk(self, instance):
        return getattr(instance, self._primary_key)

    def save_model(self, instance, form, adding=False):
        form.populate_obj(instance)
        if adding:
            self.session.add(instance)
        self.session.commit()
        return instance

    def delete_models(self, *pks):
        # Allow for cascading
        for obj in self.get_objects(*pks):
            self.session.delete(obj)
        self.session.commit()
        return True

    def construct_search(self, field_name):
        if field_name.startswith('^'):
            return literal(field_name[1:]).startswith
        elif field_name.startswith('='):
            return literal(field_name[1:]).op('=')
        else:
            return literal(field_name).contains

    def model_with_filter(self, filter_):
        m = self.model()
        if filter_:
            first, pk = filter_.split("=", 1)
            model, attr = first.split(".", 1)

            if attr.endswith("_id"):
                attr = attr[:-3]

            relationship = getattr(self.model, attr)
            obj = self.session.query(relationship.mapper.class_).get(pk)
            setattr(m, attr, obj)

        for prop in inspect(self.model).iterate_properties:
            p = getattr(self.model, prop.key)
            generator = getattr(p, "generator", None)
            if generator:
                setattr(m, prop.key, generator())
            if getattr(p, 'default', None):
                setattr(m, prop.key, p.default.arg)

        return m

    def get_list(self, page=0, sort=None, sort_desc=None, execute=False, search_query=None, filter_=None):
        qs = self.get_queryset()

        if filter_:
            qs = qs.filter(filter_)

        # Filter by search query
        if search_query and self.search_fields:
            orm_lookups = [self.construct_search(str(search_field))
                           for search_field in self.search_fields]
            for bit in search_query.split():
                or_queries = [orm_lookup(bit) for orm_lookup in orm_lookups]
                qs = qs.filter(sum(or_queries))

        count = qs.count()

        #Order queryset
        if sort:
            if sort_desc:
                sort = desc(sort)
            qs = qs.order_by(sort)

        # Pagination
        if page is not None:
            qs = qs.offset(page * self.list_per_page)

        qs = qs.limit(self.list_per_page)

        if execute:
            qs = qs.all()

        return count, qs

    def get_tabs(self, instance):
        tabs = []
        for tab in self.get_tab_names():
            relationship = inspect(getattr(self.model, tab))
            target = relationship.property.target.key

            use = relationship.expression.right
            if relationship.expression.right.primary_key:
                use = relationship.expression.left

            f = "{0}.{1}={2}".format(use.table.key, use.key,
                inspect(instance).identity[0])

            """
            # Possibly useful for more complex relationships?

            expression = relationship.expression

            ident = zip((k.key for k in meta.primary_key),
                inspect(instance).identity)

            q = json.dumps((
                ("join", meta.tables[0].key, ident),
                ("filter", str(expression))
            ))
            """

            url = url_for("{0}.list".format(target.replace("_", "")),
                filter=f)
            name = getattr(relationship.property.mapper.class_,
                "__pretty_name__", inflection.titleize(tab))
            plural = getattr(relationship.property.mapper.class_,
                "__pretty_name_plural__", inflection.pluralize(name))
            tabs.append((plural, url))

        return tabs
